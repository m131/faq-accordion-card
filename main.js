'use strict';

document.addEventListener('DOMContentLoaded', () => {
const $toggles = document.querySelectorAll('.toggle');
$toggles.forEach( item => {
		item.addEventListener('click', e =>{
			e.preventDefault();
			$toggles.forEach( el => {
				el.parentNode.classList.remove('question__title--active');
			});
			item.parentNode.classList.add('question__title--active');
		});
	});
});
